export const state = () => ({
  progress: 0
})

export const mutations = {
  increment(state) {
    state.progress++
  }
}